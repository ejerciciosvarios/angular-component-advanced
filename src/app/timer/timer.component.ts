import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { of, Subscription } from 'rxjs';
import { TimerService } from './timer.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss'],
  providers: [TimerService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimerComponent implements OnInit, OnDestroy {

  @Output() onComplete = new EventEmitter<void>();
  @Input() init:number = 20;
  private countdownEndSubscription: Subscription | null = null;
  private countdownSubscription: Subscription | null = null;

  public countdown:number = 0;

  get progreso(){
    console.log("GET PROGRESO")
    return (this.init-this.countdown)/this.init*100;
  }


  constructor(public timer:TimerService, private codRef:ChangeDetectorRef) { }

  ngOnInit(): void {
    this.timer.restartCountdown(this.init);
    console.log(this.timer.countdown$);

    this.countdownEndSubscription = this.timer.countdownEnd$.subscribe(() => {
      console.log("-- countdown ends --");
      this.onComplete.emit();
    });

    this.countdownSubscription = this.timer.countdown$.subscribe((e) => {
      this.countdown = e;
      this.codRef.markForCheck();
    });
  }

  ngOnDestroy(): void {
    this.timer.destroy();
    this.countdownEndSubscription?.unsubscribe();
    this.countdownSubscription?.unsubscribe();
  }
}
