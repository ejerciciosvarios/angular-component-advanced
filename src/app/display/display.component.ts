import { Component, Input, Output, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-display',
  templateUrl: 'display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent implements OnChanges{

  @Input() time:number | null = 0;
  public minutes:string = "00";
  public seconds:string = "00";

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes["time"]){
      const min = Math.trunc(changes["time"].currentValue / 60);
      const seg = changes["time"].currentValue - (min * 60);
      
      this.minutes = min < 10 ? "0"+min : min+ "";
      this.seconds = seg < 10 ? "0"+seg : seg+ "";

    }
  }

}
